# LöveFTW#

LöveFTW (short for "Löve For The Windows", clever right?) is a port of the [Löve](http://www.love2d.org) game development framework, to target Windows Apps (currently 8.1 Universal Apps).

For discussion, please see [this thread](https://love2d.org/forums/viewtopic.php?f=12&t=80038)!


## Usage ##

#### Requirements ####
* A Windows 8.1 (or better) PC
* Visual Studio 2013 (or better)
* A program that can clone git repositories. I recommend [SourceTree](http://www.sourcetreeapp.com/).

Note that if you want to use the Windows Phone emulator, it has its own [requirements](https://msdn.microsoft.com/en-us/library/windows/apps/ff626524%28v=vs.105%29.aspx). 

#### Instructions ####
1. Clone this repository
2. Open LoveFTW/LoveFTW.sln in Visual Studio 2013
3. Build for your target platform. Löve's "no game" screen should display if everything worked.

To add your own game:

4. In Visual Studio, identify the Solution Explorer (typically on the right side of the window). Expand LoveFTW.Shared.
5. In Windows Explorer, get your .love-file and rename it "game.love". Drag-and-drop it onto LoveFTW.Shared in Visual Studio's solution explorer.
6. Right-click on "game.love" and select "Properties".
7. Near the top of the window, set "Configuration" to "All Configurations" and "Platform" to "All Platforms".
8. On the left side of the window, navigate to Configuration Properties->General.
9. Change "Content" to "Yes". 
10. Hit "OK".
11. Build the project.
12. If everything works, modify the .appxmanifest files (LoveFTW.Windows/Package.appxmanifest and LoveFTW.WindowsPhone/Package.appxmanifest) with your own images, game name, developer name etc.

## Notes ##
* This should be considered a "preview" build. There are only a few known bugs, but it needs further testing. All feedback is welcome!

* Some corners were cut in this port. Playing .mp3 and .mod music is currently not supported, so stick with .ogg. DevIL is not included, so obscure image formats don't work either; stick with .png and .jpg images. Other than that, everything should (hopefully) work as expected.

* LöveFTW uses "standard" Lua 5.1.5, not LuaJIT. This means no ffi support etc.

* You are free to use a different name for your .love file, other than game.love, if you wish. If so, you need to modify LoveFTW.Shared/love-mobile-common/love.cpp, and change this line to whatever name you prefer.

```
#!c++

char* lovefile = "game.love";
```


* As of right now, LöveFTW forces your game to run in landscape mode only. 

* Since Windows Phone is pretty restrictive when it comes to memory, if you want to target Windows Phone, you should probably optimize your apps on memory consumption and use Visual Studio's "Performance and Diagnostics" to check your game's memory usage. 


### Performance ###
In my experiments, a "typical" Löve game runs at around 30-40 FPS on my aging Lumia 920. On PCs, I've yet to see any drops below 60 FPS.


### Known issues ###
* Performance on Windows Phone is currently limited by an [ANGLE issue](https://github.com/MSOpenTech/angle/issues/25). This should be possible to work around at some point.
* After hitting a "Resuming..." screen, audio doesn't reinitialize itself.

Found another bug/isse? Or have a feature suggestion? Let me know!

### License ###
LöveFTW is released under the zlib license. It uses multiple software libraries, each covered by different licenses. Each library is bundled with text files detailing their individual licensing.

### Special thanks ###
To SDL developer David Ludwig, for porting SDL to WinRT, and helping and supporting me a ton.

To Löve developers slime and bartbes, for not getting mad at all my stupid questions.